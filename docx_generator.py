from docx import Document
from docx.shared import Inches, RGBColor
import os, sys

if "report_generation" in os.getcwd():
    sys.path.append('../')
    parent_dir: str = ".."
else:
    parent_dir: str = ""

from helper.settings import (min_mos, max_mos)

dic_res = {'2020-07-19': {'#_measurements': 1357, '#_worst_mos': 19, '#_best_most': 18, 'percentage_worst_mos': 1.4, 'percentage_best_mos': 1.326, '#_alerts': 25, '#_mean': 3.6887324981577008}, '2020-07-26': {'#_measurements': 1361, '#_worst_mos': 6, '#_best_most': 9, 'percentage_worst_mos': 0.441, 'percentage_best_mos': 0.661, '#_alerts': 2, '#_mean': 3.7438280675973545}}
dic_alerts = {'2020-07-19': {'warning_MOS': 6, 'alert_Blurring': 5, 'warning_Blurring': 14}, '2020-07-26': {'warning_MOS': 1, 'warning_Blurring': 1}}
canales = ["2020-07-19", "2020-07-26"]
filename_full_path = "../documents/video_on_demand/dorna/figures"

def gen_docx(canales, filename_full_path, dic_alerts, dic_res):


    figura = 1
    
    lista_canales = ''
    for canal in canales:
        lista_canales += canal + ", "
    lista_canales = lista_canales[0:-2]+"."

    document = Document()

    document.add_heading('Informe VideoMOS', 0)
    # Cambiar aquí
    p = document.add_paragraph(style = 'Subtitle')
    p.add_run('Para los canales: ' + lista_canales).bold = True

    document.add_heading('1. Introducción', level=1)
    # Cambiar aquí
    p = document.add_paragraph('Este documento recoge los resultados obtenidos por el sistema de medición de calidad subjetiva de vídeo para los canales: ')
    p.add_run(lista_canales).bold=True

    p = document.add_paragraph("El sistema de medición de calidad subjetiva proporciona un valor de calidad continuo comprendido entre 1 y 5, correspondiente a la escala MOS (Mean Opinion Score por sus siglas en inglés) la cual es muy utilizada en la realización de pruebas subjetivas de vídeo con observadores reales (ITU-R BT.500 ")
    p.add_run("Metodologías para la evaluación subjetiva de la calidad de imágenes de televisión").italic = True
    p.add_run(").")
    document.add_paragraph("Para obtener dicha medida, el sistema realiza un análisis, en tiempo real, tanto de las características formales de la señal audiovisual (codificador, resolución, etc.) como del contenido de la misma (texturas, gradientes, movimiento, etc.). En función de estos datos, se realiza una predicción del valor de MOS utilizando modelos de inteligencia artificial para dicha tarea.")
    document.add_paragraph("Además del valor de MOS, el sistema proporciona un conjunto de avisos y alertas, cuando la señal supera ciertos umbrales. Típicamente, los umbrales son configurables por el usuario, lo que permite al radiodifusor tener una monitorización completa y detallada de todo lo sucedido durante cada jornada.")
    document.add_paragraph("El informe está compuesto de dos secciones. Una primera sección que recopila, de manera detallada, el análisis individual de cada uno de los canales en términos de MOS y alertas recibidas durante el intervalo de tiempo monitorizado, y una segunda sección que muestra una valoración comparativa de todos los canales analizados.")



    document.add_heading("2. Informe por Canal", level=1)
    # Cambiear aquí
    document.add_paragraph("El objetivo de esta sección es reportar los resultados del análisis de la calidad de la señal de vídeo para los canales: "+ lista_canales)

    document.add_paragraph("En particular, esta sección cuenta con dos partes principales para cada uno de los canales: la primera está enfocada a reportar los resultados de las métricas de calidad subjetiva MOS a lo largo del intervalo de tiempo considerado. Por otro lado, dicho informe refleja una segunda parte donde se muestran el conjunto de alertas que se obtuvieron en el intervalo de tiempo analizado y que permiten al usuario investigar, de una manera eficiente y sencilla, momentos de la transmisión en donde se detectaron posibles anomalías de calidad en la señal. \n A continuación, se muestran gráficamente los resultados de un programa asociado a un evento a través de las Figuras 1, 3 y 5. Dicha representación se extrae del Dashboard general que incorpora la web del sistema. En particular, se incluye tanto el valor instantáneo de cada medida, como el valor medio de cada programa. Para complementar dicho análisis, se incluye además un gráfico circular o “de pastel” con los porcentajes de calidad subjetiva a lo largo de un programa. \n Además, las Figuras 2, 4 y 6 representan las distribuciones individuales del MOS para el intervalo de tiempo considerado. En particular, el eje x refleja la escala de MOS, con valores entre 1 y 5, mientras que el eje y representa la función de densidad de aparición de cada valor que muestra un kernel Gaussiano. Dichas distribuciones se aproximan como distribuciones Gaussianas, con valores medios indicados con la letra griega 𝜇, y su desviación típica representada con la letra griega σ.")

    n_canal = 1
    for canal in canales:
        # Cambiar canal
        document.add_heading("2."+str(n_canal)+". Canal "+canal, level=2)
        n_canal += 1
        # Meter Imagen correspondiente
        document.add_picture('fake_captura.png', width=Inches(6))
        p = document.add_paragraph()
        p.add_run("Figura "+str(figura)+".  Representación del Dashboard para la monitorización de la métrica subjetiva de calidad MOS para el canal 24h HD. ").italic = True
        figura +=1
        # Meter Imagen correspondiente
        figure = os.path.join(filename_full_path, canal + "_histogram.png")
        document.add_picture(figure, width=Inches(6))
        p = document.add_paragraph()
        p.add_run("Figura "+str(figura)+". Representación de la distribución de la métrica subjetiva de calidad MOS para el canal 24h HD ").italic = True
        figura +=1



    document.add_heading("3. Informe por Comparativo", level=1)
    document.add_paragraph("Finalmente, esta sección está centrada en el análisis comparativo de los tres canales monitorizados con el propósito de facilitar la visualización de la comparación de calidad de la señal en los distintos contenidos, en términos de experiencia de usuario, durante el intervalo de tiempo analizado. \n En la Figura 7, se refleja la distribución de MOS para los distintos eventos previamente descritos. Nuevamente, el eje x refleja la escala de MOS, con valores entre 1 y 5, mientras que el eje y representa la función de densidad de aparición de cada valor, mostrando un kernel Gaussiano. El título de la Figura 7 indica los parámetros de media (indicado por la letra μ) y desviación típica (𝜎). \n Este tipo de representación permite analizar y comparar, de manera eficiente, la calidad de la señal de vídeo, en un intervalo temporal dado, para los diferentes eventos, viendo los valores medios y desviaciones en cada uno de ellos, ya que se trata de una representación global y no temporal de la señal.  ")
    # Meter Imagen correspondiente
    filename = os.path.join(filename_full_path, "multiple_histogram.png")
    document.add_picture(filename, width=Inches(6))
    p = document.add_paragraph()
    p.add_run("Figura 7.  Representación de la distribución de la métrica subjetiva de calidad MOS para todos los canales analizados en el informe.").italic = True

    p = document.add_paragraph("Por otro lado, en la Figura 9 se representa la distribución de la medida subjetiva MOS para los distintos eventos utilizando un ")
    p.add_run("Box-plot").italic = True
    p.add_run(" o ")
    p.add_run("diagrama de bigotes").italic = True
    p.add_run(" como se denomina en castellano. En dicha representación, la distribución se presenta como una caja, donde se visualizan los valores de los cuartiles (intervalos que distribuyen los valores de MOS en 4 segmentos, según su densidad de aparición), los extremos (máximo y mínimo), el valor estadístico de la mediana (que divide el espacio muestral en dos partes con el mismo número de muestras) y los denominados valores atípicos o ")
    p.add_run("outliers.")
    p = document.add_paragraph("Para facilitar el entendimiento de la representación del ")
    p.add_run("Box-plot").italic = True
    p.add_run(", se incluye a continuación la Figura 8 donde se indican los distintos parámetros de dicha representación.")
    # Meter imagen correspondiente
    document.add_picture('box_plot_explanation.png', width=Inches(6))
    p = document.add_paragraph()
    p.add_run("Figura 8. Representación de los diferentes parámetros involucrados en un Box-plot incluidos cuartiles (q1, q2 o mediana y q3) así como los valores extremos (máximo y mínimo) y atípicos. ").italic = True
    filename = os.path.join(filename_full_path, "multiple_boxplot.png")
    document.add_picture(filename, width=Inches(6))
    p = document.add_paragraph()
    p.add_run("Figura 9. Visualización de la distribución de la métrica subjetiva de calidad MOS para los distintos canales analizados utilizando una representación Boxplot. El valor medio se representa con el número recuadrado y posicionado según su valor para facilitar su comparación con la mediana. ").italic = True

    # Cambiar aquí los canales
    document.add_paragraph("Por último, en las siguientes tablas se recogen los valores más extremos de MOS detectados y los principales valores estadísticos sobre las alertas generadas (y su naturaleza) tras el estudio realizado sobre los "+lista_canales)

    table = document.add_table(rows=len(canales)+1, cols=6)
    table.cell(0, 0).text = "Canales"
    table.cell(0, 1).text = "# medidas "
    table.cell(0, 2).text = "# medidas ≤ "+str(min_mos)
    table.cell(0, 3).text = "# medidas ≥ "+str(max_mos)
    table.cell(0, 4).text = "% medidas ≤ "+str(min_mos)
    table.cell(0, 5).text = "% medidas ≥ "+str(max_mos)
    for canal_ind, canal in enumerate(canales):
        table.cell(canal_ind+1, 0).text = canal
        table.cell(canal_ind+1, 1).text = str(dic_res[canal]['#_measurements'])
        table.cell(canal_ind+1, 2).text = str(dic_res[canal]['#_worst_mos'])
        table.cell(canal_ind+1, 3).text = str(dic_res[canal]['#_best_most'])
        table.cell(canal_ind+1, 4).text = str(dic_res[canal]['percentage_worst_mos'])
        table.cell(canal_ind+1, 5).text = str(dic_res[canal]['percentage_best_mos'])

    table.style = 'Light Shading Accent 1'
    p = document.add_paragraph()
    p.add_run("Tabla 1. Principales estadísticas de los eventos analizados en términos de MOS y Alertas generadas. (símbolo # indica número; símbolo % indica el porcentaje).").italic = True

    table = document.add_table(rows=3, cols=2)
    table.cell(0, 0).text = "Criterio"
    table.cell(1, 0).text = "MOS medio más alto "
    table.cell(2, 0).text = "MOS medio más bajo "
    table.cell(0, 1).text = "Canal"
    peor_mos = 5
    mejor_mos = 0
    for canal in dic_res:
        if dic_res[canal]["#_mean"]<peor_mos:
            peor_canal = canal
            peor_mos = dic_res[canal]["#_mean"]
        if dic_res[canal]["#_mean"]>mejor_mos:
            mejor_canal = canal
            mejor_mos = dic_res[canal]["#_mean"]
    
    table.cell(1, 1).text = mejor_canal
    table.cell(2, 1).text = peor_canal
    table.style = 'Light Shading Accent 1'
    p = document.add_paragraph()
    p.add_run("Tabla 2. Principales criterios de evaluación considerando todos los eventos. ")


    table = document.add_table(rows=10, cols=len(canales)+1)
    table.cell(0, 0).text = "Canal"
    table.cell(1, 0).text = "#warnings "
    table.cell(2, 0).text = "Frame Loss"
    table.cell(3, 0).text = "Signal Loss"
    table.cell(4, 0).text = "Content Loss"
    table.cell(5, 0).text = "Block Loss"
    table.cell(6, 0).text = "Blurring"
    table.cell(7, 0).text = "Freezing "
    table.cell(8, 0).text = "Darkness "
    table.cell(9, 0).text = "Low MOS"
    alerts = ["warning_LostFrames", "warning_LossSignal", "warning_LossContent", "warning_Blockloss", "warning_Blurring", "warning_Freezing", "warning_Darkness", "warning_MOS"]
    for canal_ind, canal in enumerate(canales):
        table.cell(0, canal_ind+1).text = canal
        n_alerts = 0
        for n_alert, alert in enumerate(alerts):
            txt = ""
            if alert in dic_alerts[canal]:
                n_alerts += dic_alerts[canal][alert]
                txt += str(dic_alerts[canal][alert])
            else:
                txt += "-"
            table.cell(n_alert+2, canal_ind+1).text = txt
        table.cell(1, canal_ind+1).text = str(n_alerts)
    table.style = 'Light Shading Accent 1'
    p = document.add_paragraph()
    p.add_run("Tabla 3. Número de warning generados para cada uno de los canales.")



    table = document.add_table(rows=10, cols=len(canales)+1)
    table.cell(0, 0).text = "Canal"
    table.cell(1, 0).text = "#alertas "
    table.cell(2, 0).text = "Frame Loss"
    table.cell(3, 0).text = "Signal Loss"
    table.cell(4, 0).text = "Content Loss"
    table.cell(5, 0).text = "Block Loss"
    table.cell(6, 0).text = "Blurring"
    table.cell(7, 0).text = "Freezing "
    table.cell(8, 0).text = "Darkness "
    table.cell(9, 0).text = "Low MOS"
    alerts = ["alert_LostFrames", "alert_LossSignal", "alert_LossContent", "alert_Blockloss", "alert_Blurring", "alert_Freezing", "alert_Darkness", "alert_MOS"]
    for canal_ind, canal in enumerate(canales):
        table.cell(0, canal_ind+1).text = canal
        n_alerts = 0
        for n_alert, alert in enumerate(alerts):
            txt = ""
            if alert in dic_alerts[canal]:
                n_alerts += dic_alerts[canal][alert]
                txt += str(dic_alerts[canal][alert])
            else:
                txt += "-"
            table.cell(n_alert+2, canal_ind+1).text = txt
        table.cell(1, canal_ind+1).text = str(n_alerts)
    table.style = 'Light Shading Accent 1'
    p = document.add_paragraph()
    p.add_run("Tabla 4. Número de alertas generadas para cada uno de los canales.")
    
    document.add_heading("4. Conclusiones", level=1)

    run = document.add_paragraph().add_run("Como se puede observar en las gráficas de análisis individual de cada jornada, en la sección 2 de este informe, la codificación de la información de vídeo se ha llevado a cabo de manera muy similar en todos los casos, con valores medios de MOS entre 2,4 y 2,5, es decir, con valores en la categoría de “Pobre” (2 a 2,5), con fluctuaciones que son normales (entre 0,31 y 0,38) en retransmisiones con este tipo de contenidos, y que provocan un muy pequeño porcentaje de valores de MOS “Malo” (<2) y en torno a un 25% de valores que superan el umbral de 2,5 y alcanzan la categoría de “Regular” (de 2,5 a 3,0). Todas las distribuciones presentan una cierta simetría en torno a la media, lo cual pone de manifiesto una razonable uniformidad de los contenidos analizados. Los valores medios de MOS sugieren la conveniencia de incrementar la velocidad binaria objetivo a los codificadores con el fin de poder mejorar la experiencia de usuario y evitar los valores de la categoría de “Pobre” y los valores de MOS correspondientes a categorías de “Malo” (valores inferiores a 2,0) y se aleje de valores atípicos de la categoría de “Molesto” (valores en torno a 1,0).  \nSe observa una gran similitud en todas las campanas, lo cual pone de manifiesto que la codificación se ha realizado en condiciones y con parámetros muy similares. La similitud entre los valores medios y las medianas, según la gráfica de la figura 15, confirma que la asimetría de las campanas es muy pequeña, poniendo de manifiesto la uniformidad de los contenidos analizados. Sería necesario un incremento importante en la velocidad binara objetivo de los codificadores para lograr mejorar la percepción visual del vídeo transmitido. \n Con respecto a las alertas generadas, los valores obtenidos son coherentes con las valoraciones medias de MOS ofrecidas por la sonda. Destaca, de manera general y en todas las jornadas analizadas, el alto porcentaje (superior en todos los casos al 77%) de medidas con valores medios de MOS por debajo de 2,75, causadas por valores bajos de MOS o por excesiva borrosidad del contenido de la imagen. Dicho porcentaje oscila entre el 77% y el 82%, lo cual implica que tanto los contenidos analizados como las condiciones de compresión de la señal han sido, en todos los casos, muy similares.  ")
    run.font.color.rgb = RGBColor(0xff, 0x00, 0x00)
    document.save('informe.docx')

# gen_docx(canales, filename_full_path, dic_alerts, dic_res)