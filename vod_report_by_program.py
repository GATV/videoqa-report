import os, sys
import pandas as pd

if "report_generation" in os.getcwd():
    sys.path.append('../')
    parent_dir: str = ".."
else:
    parent_dir: str = ""

from helper.report_utils import (load_report_data_from_folder,
                                 get_full_data_path,
                                 get_labels_colors_from_pandas_column,
                                 plot_multiple_histograms,
                                 plot_multiple_boxplots,
                                 get_full_figures_path,
                                 generate_individual_analytics,
                                 get_temporal_data,
                                 plot_temporal_analytics,
                                 show_alert_statistics)

from helper.settings import (extension, vod_directory,
                             documents_dir, palette,
                             grouped_col, target_col,
                             figures_folder, save_figures,
                             time_col, min_mos, max_mos,
                             analysis_dir, journey_col)
from docx_generator import gen_docx

# 1. Generate data path
data_folder: str = get_full_data_path(
    parent_dir=parent_dir,
    documents_dir=documents_dir,
    report_type_dir=vod_directory,
    analysis_dir=analysis_dir)

# 2. Extract figure data paths
figure_filename_full_path: str = get_full_figures_path(
    data_folder=data_folder,
    figures_folder=figures_folder)
# 3. Retrieve data
df: pd.DataFrame = load_report_data_from_folder(
    data_folder=data_folder,
    extension=extension,
    reset_index=True,
    journey_col=journey_col,
    preprocess_journey=True)


df_temp: pd.DataFrame = get_temporal_data(
    data_folder=data_folder,
    extension=extension,
    timestamp_index=True,
    n_sampling=3)

# 4. Retrieve labels and additional parameters to plot figures
data_labels, canales = get_labels_colors_from_pandas_column(
    df=df, column=grouped_col, palette=palette)
print(figure_filename_full_path)

# 5. Extract histograms
plot_multiple_histograms(data=df, grouped_col=grouped_col, data_labels=data_labels,
                         target_col=target_col, save=save_figures,
                         filename_full_path=figure_filename_full_path, plot_title=journey_col)
# 6. Extract Box-plots
plot_multiple_boxplots(data=df, grouped_col=grouped_col,
                       target_col=target_col, save=save_figures,
                       filename_full_path=figure_filename_full_path,
                       palette=palette)

# 7. Extract Individual Information
generate_individual_analytics(
    data=df, grouped_col=grouped_col,
    data_labels=data_labels,
    target_col=target_col,
    save_figures=save_figures,
    filename_full_path=figure_filename_full_path)


# 8. Temporal report
plot_temporal_analytics(
    data=df_temp,
    grouped_col=grouped_col,
    time_col=time_col,
    target_col=target_col,
    save_figure=save_figures,
    filename_full_path=figure_filename_full_path)

# 9. Alerts Report
dic_res, dic_alerts = show_alert_statistics(
    df=df,
    grouped_col=grouped_col,
    target_col=target_col,
    data_labels=data_labels,
    min_mos=min_mos,
    max_mos=max_mos,
    decimals=3)


gen_docx(canales, figure_filename_full_path, dic_alerts, dic_res)