import os, glob
import itertools, operator
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from helper.settings import logger, grouped_col
from collections import OrderedDict


def prepare_directory(directory_path: str):
    try:
        if not os.path.exists(directory_path):
            os.makedirs(directory_path)
    except Exception as e:
        logger.error(e)


def get_full_data_path(parent_dir: str, documents_dir: str, report_type_dir: str, analysis_dir: str):
    full_data_path: str = os.path.join(parent_dir, documents_dir, report_type_dir, analysis_dir)
    return full_data_path


def load_report_data_from_folder(data_folder: str, timestamp_index=True,
                                 extension: str = ".csv", reset_index=True,
                                 preprocess_journey: bool = True,
                                 journey_col: str = "journey_date"):
    data_report: pd.DataFrame = pd.DataFrame()
    try:
        converters = {"alerts": lambda x: x.strip("[]").replace("'", "").split(", ")}

        for data_file in glob.glob(data_folder + "/*" + extension):
            if timestamp_index:
                index_col = 0
            else:
                index_col = None

            temp_data = pd.read_csv(data_file, index_col=index_col, converters=converters)
            data_report = pd.concat([data_report, temp_data], axis=0, sort=False)

        if preprocess_journey:
            data_report[journey_col] = data_report.apply(
                preprocess_journey_date,
                axis=1, args=(journey_col,))

        if reset_index:
            data_report: pd.DataFrame = data_report.reset_index(drop=False)
    except Exception as e:
        logger.error(e)
    return data_report


def get_full_figures_path(data_folder: str, figures_folder: str):
    full_figures_path: str = ""
    try:
        full_figures_path: str = os.path.join(data_folder, figures_folder)
        prepare_directory(directory_path=full_figures_path)
    except Exception as e:
        logger.error(e)
    return full_figures_path


def get_labels_colors_from_pandas_column(df: pd.DataFrame, column: str, palette: str):
    data_labels: dict = dict()
    try:
        labels: list = df[column].unique().tolist()
        colors: list = sns.color_palette(palette, len(labels))
        data_labels: dict = dict(zip(labels, colors))
    except Exception as e:
        logger.error(e)
    return data_labels


def preprocess_journey_date(row, col_name):
    return row[col_name].split(" ")[0]


def plot_multiple_histograms(data: pd.DataFrame,
                             grouped_col: str,
                             target_col: str,
                             data_labels: dict, save=False,
                             filename_full_path: str = None,
                             plot_title: str = ""):
    # Plot
    plt.figure(figsize=(12, 10))
    title = "\n"
    labels: list = list(data_labels.keys())
    for j, i in enumerate(labels):
        x = data.loc[data[grouped_col] == i, target_col]
        mu_x = round(float(np.mean(x)), 3)
        sigma_x = round(float(np.std(x)), 3)
        ax = sns.distplot(x, color=data_labels.get(i), label=i, hist_kws=dict(alpha=.1),
                          kde_kws={'linewidth': 2})
        ax.axvline(mu_x, color=data_labels.get(i), linestyle='--')
        ax.set(xlabel='Mean Opinion Score', ylabel='Density', xlim=(1, 5))
        title += f"Parámetros {str(i)}: $G(\mu=$ {mu_x}, $\sigma=$ {sigma_x} \n"
        ax.set_title(title)
    plt.legend(title=plot_title)
    plt.grid()
    plt.tight_layout()
    if save:
        filepath: str = os.path.join(filename_full_path, "multiple_histogram.png")
        plt.savefig(filepath, bbox_inches='tight')
    # plt.show()
    plt.grid()


def plot_multiple_boxplots(data: pd.DataFrame, grouped_col: str, target_col: str,
                           save: bool = False, filename_full_path: str = None,
                           palette: str = "husl"):
    plt.figure(figsize=(12, 10))

    means: dict = data.groupby([grouped_col])[target_col].mean().to_dict(OrderedDict)
    counter: int = 0

    bp = sns.boxplot(x=grouped_col, y=target_col, data=data, palette=palette, order=list(means.keys()))
    bp.set(xlabel='', ylabel='Mean Opinion Score', ylim=(1, 5))
    ax = bp.axes

    for k, v in means.items():
        # every 4th line at the interval of 6 is median line
        # 0 -> p25 1 -> p75 2 -> lower whisker 3 -> upper whisker 4 -> p50 5 -> upper extreme value
        mean = round(v, 2)
        ax.text(
            counter,
            mean,
            f'{mean}',
            ha='center',
            va='center',
            fontweight='bold',
            size=10,
            color='white',
            bbox=dict(facecolor='#445A64'))
        counter += 1
    bp.figure.tight_layout()
    plt.grid()
    if save:
        filepath: str = os.path.join(filename_full_path, "multiple_boxplot.png")
        plt.savefig(filepath, bbox_inches='tight')
    # plt.show()
    plt.grid()


def plot_individual_histogram(data: pd.DataFrame, label: str, grouped_col: str,
                              target_col: str, color: str, save: bool = False,
                              filename_full_path: str = None):

    x = data.loc[data[grouped_col] == label, target_col]
    mu_x = round(float(np.mean(x)), 3)
    sigma_x = round(float(np.std(x)), 3)

    plt.figure(figsize=(12, 10))
    ax = sns.distplot(x, color=color, label=label)
    ax.set(xlabel='Mean Opinion Score', ylabel='Density', xlim=(1, 5))
    ax.axvline(np.mean(x), color=color, linestyle='--')
    ax.set_title(f"Parámetros: $G(\mu=$ {mu_x}, $\sigma=$ {sigma_x}")
    plt.legend()
    plt.tight_layout()

    # plt.show()
    plt.grid()
    if save:
        filepath: str = os.path.join(filename_full_path, label + "_histogram.png")
        plt.savefig(filepath, bbox_inches='tight')
    plt.grid()


def stack_data_as_list(data_folder: str, timestamp_index=False, extension: str = ".csv"):
    data_struct: list = []
    try:
        converters = {"alerts": lambda x: x.strip("[]").replace("'", "").split(", ")}

        for file in glob.glob(data_folder + "/*" + extension):
            if timestamp_index:
                index_col = 0
            else:
                index_col = None
            temp_data = pd.read_csv(file, index_col=index_col, converters=converters)
            data_struct += [temp_data]
    except Exception as e:
        print(e)
    return data_struct


def get_temporal_data(data_folder: str, timestamp_index: bool,
                      extension: str,
                      n_sampling: int):

    df: pd.DataFrame = pd.DataFrame([])
    try:
        df: pd.DataFrame = pd.DataFrame([])
        temporal_data_struct: list = stack_data_as_list(
            data_folder=data_folder,
            timestamp_index=timestamp_index,
            extension=extension)
        min_length: int = min([i.shape[0] for i
                               in temporal_data_struct])

        for i, df_temp in enumerate(temporal_data_struct):
            if df_temp.shape[0] > min_length:
                pad: int = int(df_temp.shape[0] - min_length)
                df_temp = df_temp.iloc[:-pad, :]

            df_channel = df_temp[::n_sampling]
            print(df_channel.shape)

            if i == 0:
                df = df_channel
            else:
                df = pd.concat([df, df_channel],
                               axis=0,
                               sort=False)
            df.reset_index(drop=False)
            print(df.shape)

        df = df.reset_index(drop=False)
    except Exception as e:
        logger.error(e)
    return df


def plot_temporal_analytics(data: pd.DataFrame, time_col: str, grouped_col: str,
                            target_col: str,
                            save_figure: bool = False, filename_full_path: str = None,
                            palette: str = "husl"):

    plt.figure(figsize=(12, 10))
    ax_plot = sns.lineplot(
        x=time_col, y=target_col,
        data=data, palette=palette,
        hue=grouped_col,
        marker="o", dashes=False,
        linewidth=1.5)

    ax_plot.set(xlabel='Seconds (s)', ylabel='Mean Opinion Score', ylim=(1, 5))
    plt.tight_layout()
    plt.grid()
    legend = ax_plot.legend()
    legend.texts[0].set_text("Channels")
    if save_figure:
        filepath: str = os.path.join(filename_full_path, "temporal.png")
        plt.savefig(filepath, bbox_inches='tight')
    # plt.show()
    plt.grid()


def generate_individual_analytics(data: pd.DataFrame,
                                  grouped_col: str, target_col: str,
                                  data_labels: dict, save_figures: bool,
                                  filename_full_path: str):
    for label, color in data_labels.items():
        plot_individual_histogram(
            data=data, label=label, target_col=target_col,
            grouped_col=grouped_col, color=color,
            save=save_figures, filename_full_path=filename_full_path)


def get_alerts_from_df(df: pd.DataFrame, alert_col="alerts"):
    alerts = df[[alert_col]].values.ravel().tolist()
    alerts_join: list = list(itertools.chain.from_iterable(alerts))
    n_alerts: int = len([i for i in alerts_join if i != ""])
    return n_alerts

def get_mos_statistics(df: pd.DataFrame, target_col: str, label: str, min_mos: float = 2.75,
                       max_mos: float = 4.25, decimals: int = 3):
    channel_df: pd.DataFrame = df[df[grouped_col] == label]
    worst_mos = channel_df[channel_df[target_col] <= min_mos]
    percentage_worst_mos = round(100*(worst_mos.shape[0] / channel_df.shape[0]), decimals)

    best_mos = channel_df[channel_df[target_col] >= max_mos]
    percentage_best_mos = round(100*(best_mos.shape[0] / channel_df.shape[0]), decimals)
    # Alerts
    n_alerts = get_alerts_from_df(df=channel_df)

    response: dict = {"#_measurements": channel_df.shape[0], "#_worst_mos": worst_mos.shape[0],
                      "#_best_most": best_mos.shape[0], "percentage_worst_mos": percentage_worst_mos,
                      "percentage_best_mos": percentage_best_mos, "#_alerts": n_alerts}
    return response


def show_alert_statistics(df: pd.DataFrame, grouped_col: str,
                          target_col: str, data_labels: dict,
                          min_mos: float, max_mos: float, decimals: int):
    for label, color in data_labels.items():
        res: dict = get_mos_statistics(
            df=df, target_col=target_col,
            label=label,
            min_mos=min_mos,
            max_mos=max_mos,
            decimals=decimals)
        print("Statistical Analysis for Channel: {}".format(label))
        print(res)
        print("Done")

        # Alerts
        print("Channel", label)
        data_1 = df.loc[df[grouped_col] == label]
        alerts: list = data_1.alerts.values.ravel().tolist()
        all_alerts = [a for b in alerts for a in b]
        unique_alerts = list(set(all_alerts))
        for ua in unique_alerts:
            if ua != "":
                print("Alert {}: #{}".format(ua, all_alerts.count(ua)))
        print("..........................................")