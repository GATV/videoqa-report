import coloredlogs, logging

# Create a logger object.
logger = logging.getLogger(__name__)
coloredlogs.install(level='DEBUG', logger=logger)


# Environment variables to analyse specific columns
extension: str = ".csv"
program_name: str = "program_name"
url_name: str = "url"
grouped_col: str = "journey_datetime"
journey_col: str = "journey_datetime"
target_col: str = "mos"
time_col: str = "timestamp"
palette: str = "tab10"
min_mos: float = 2.75
max_mos: float = 4.25


documents_dir: str = "documents"
vod_directory: str = "video_on_demand"
streaming_directory: str = "streaming"
figures_folder: str = "figures"
analysis_dir: str = "dorna"
save_figures: bool = True

